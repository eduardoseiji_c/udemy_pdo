<?php

    class Conexao {
        
        private $host = 'localhost';
        private $dbname = 'udemy_pdo';
      //private $dbname = 'eduardo_udemy_pdo';
        private $user = 'root';
        private $pass = '';
        public function conectar() {
            try {
                $conexao = new PDO(
                    "mysql:host=$this->host;dbname=$this->dbname",
                    "$this->user",
                    "$this->pass"
                );

                return $conexao;
            } catch(PDOException $e) {
                echo "<pre>";  
                echo $e->getMessage();
                echo "</pre>";
            }
        }
    }