<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    require '../app_lista_tarefa/tarefa.model.php';
    require '../app_lista_tarefa/tarefa.service.php';
    require '../app_lista_tarefa/Conexao.php';

    $acao = isset($_GET['acao']) ? $_GET['acao'] : $acao;

    echo $acao;

    if($acao == 'inserir') {

        $tarefa = new Tarefa();
        $tarefa->__set('tarefa', $_POST['tarefa']);

        $conexao = new Conexao();

        $tarefaService = new TarefaService($conexao, $tarefa);
        $tarefaService->insert();

        header("Location: nova_tarefa.php?inclusao=1");

    } else if($acao == 'recuperar') {
        echo 'Chegamos até aqui!';

        $tarefa = new Tarefa();
        $conexao = new Conexao();

        $tarefaService = new TarefaService($conexao, $tarefa);
        $tarefas = $tarefaService->read();

    } else if ($acao == 'atualizar') {
        $tarefa = new Tarefa();
        $tarefa->__set('id', $_POST['id'])
               ->__set('tarefa', $_POST['tarefa']);
		
		 
		$conexao = new Conexao();

		$tarefaService = new TarefaService($conexao, $tarefa);
		$tarefaService->update();
        if($tarefaService->update()) {
            header("Location: todas_tarefas.php");
        }
    } else if ($acao == 'deletar') {
        $tarefa = new Tarefa();
        $tarefa->__set('id', $_GET['id']);
        $conexao = new Conexao();

        $tarefaService = new TarefaService($conexao, $tarefa);
        $tarefaService->delete();
        header('Location: todas_tarefas.php');

    } else if ($acao == 'marcarRealizada') {
        $tarefa = new Tarefa();
        $tarefa->__set('id', $_GET['id'])->__set('id_status', 2);

        $conexao = new Conexao();

        $tarefaService = new TarefaService($conexao, $tarefa);
        $tarefaService->marcarRealizada();

        header('Location: todas_tarefas.php');

    }